//@prepros-append utils.js
//@prepros-append main.js
'use strict'

class Utils {

    static Click(element, toDo){
        $(document).on('click', element, toDo);
    }

}
'use strict'

class ViewStack {
    constructor(){
        this.stack = [];
    }

    removeView(){
        this.stack.pop();
    }

    addView(view){
        this.stack.push(view);
    }

    peekView(){
        return this.stack[this.stack.length - 1];
    }
}
'use strict'

class View{
    constructor(id, isOpen){
        this.id = id;
        this.isOpen = isOpen;
        if (this.isOpen == true) {
            this.showView();

        }
    }
    isOpen(){
        return this.isOpen;
    }

    showView(){
        if (this.isOpen == false) {
            $('#' + this.id).css('transform', 'translateX(0%)');
            viewStack.addView(this);
            this.isOpen = true;
        }
        else{
            console.log(this.id + " is already open and on the view stack");
        }
    }

    hideView(){
        $('#' + this.id).css('transform', 'translateX(100%)');
        viewStack.removeView();
        this.isOpen = false;
    }
}
$(document).ready(function(){
    var mainView = new View('mainView', true);
    var secondView = new View('secondView', false);
    var thirdView = new View('thirdView', false);
    var fourthView = new View('fourthView', false);

    Utils.Click('li', function(){
        switch ($(this).text()) {
            case "Back":
                console.log("Back");
                console.log(viewStack);
                viewStack.peek().hideView();
                break;
            case "One":
                console.log("One");
                mainView.showView();
                break;
            case "Two":
                secondView.showView();
                console.log("Two");
                break;
            case "Three":
                thirdView.showView();
                console.log("Three");
                break;
            case "Four":
                fourthView.showView();
                console.log("Fourth");
                break;
            default:
                break;
        }
        console.log(viewStack);
    });



    // $(document).on('click', 'li', function(){
    //     switch ($(this).text()) {
    //         case "Back":
    //             console.log("Back");
    //             console.log(viewStack);
    //             viewStack.peekView().hideView();
    //             break;
    //         case "One":
    //             console.log("One");
    //             mainView.showView();
    //             break;
    //         case "Two":
    //             secondView.showView();
    //             console.log("Two");
    //             break;
    //         case "Three":
    //             thirdView.showView();
    //             console.log("Three");
    //             break;
    //         case "Four":
    //             fourthView.showView();
    //             console.log("Fourth");
    //             break;
    //         default:
    //             break;
    //     }
    //     console.log(viewStack);
    // });

});

var viewStack = new ViewStack();
